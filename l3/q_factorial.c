#include <stdio.h>
#include <stdlib.h>

int main(int argc, char *argv[])
{
        long long int factorial = 1;
        int start = 0;

        if (argc < 2) {
            printf("%s", "Specify a number\n");
            exit(1);
        }

        start = strtol(argv[1], NULL, 0);
        factorial = strtol(argv[1], NULL, 0);

        while (start > 1) {
            start--;
            factorial = factorial * start;     
        }


        printf("%lld\n", factorial);
}
