#!/usr/bin/env python3

import sys

def print_words(words):
    for word in words:
        print(word)

# TODO Complete this function
def main():
    all_words = []

    sys.argv.pop(0)

    for line in sys.stdin:
        words = line.split(',')
        for word in words:
            word = word.strip()
            if (word not in all_words):
                all_words.append(word)
    
    all_words = sorted(all_words)
    shortest_words = []
    longest_words = []
    shortest_word_len = len(min(all_words, key=len))
    longest_word_len = len(max(all_words, key=len))

    for word in all_words:
        if len(word) is shortest_word_len:
            shortest_words.append(word)
        if len(word) is longest_word_len:
            longest_words.append(word)

    print("All word in alphabetical order:")
    print_words(all_words)
    print("Shortest words:")
    print_words(shortest_words)
    print("Longest words:")
    print_words(longest_words)

if __name__ == "__main__":
    main()
