#define _XOPEN_SOURCE
// I had to define this in order to get string.h working.
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <time.h>
#include <ctype.h>

#define MAX_LINE_LEN 200
#define MAX_EVENTS 1000

// parse_date
// purpose: takes user-inputted date strings and returns the corresponding tm struct.
// parameters:
//     date: the user-inputted date string.
// returns: the tm struct from the date string data.
struct tm parse_date(char* date) {
    struct tm tm = { 0 };

    tm.tm_isdst = -1;
    strptime(date, "%Y/%m/%d", &tm);

    return tm;
}

// month_name
// purpose: takes the one-based index of a month and sets the destination string to the corresponding month.
// parameters:
//     month_num: the one-based index of a month.
//     destination: the destination for the month string.
// returns: 0. no errors are expected from this function.
int month_name(int month_num, char destination[10]) {
    switch(month_num) {
        case 1:
            strcpy(destination, "January\0");
            break;
        case 2:
            strcpy(destination, "February\0");
            break;
        case 3:
            strcpy(destination, "March\0");
            break;
        case 4:
            strcpy(destination, "April\0");
            break;
        case 5:
            strcpy(destination, "May\0");
            break;
        case 6:
            strcpy(destination, "June\0");
            break;
        case 7:
            strcpy(destination, "July\0");
            break;
        case 8:
            strcpy(destination, "August\0");
            break;
        case 9:
            strcpy(destination, "September\0");
            break;
        case 10:
            strcpy(destination, "October\0");
            break;
        case 11:
            strcpy(destination, "November\0");
            break;
        case 12:
            strcpy(destination, "December\0");
            break;
    }
    return 0;
}

// twelve_hour
// purpose: converts a 24 hour time into a 12 hour time string.
// parameters:
//     hour: the input time hour
//     minute: the input time minute
//     destination: the destination string for the formatted output.
// returns: 0 on success. no errors are expected from this function.
int twelve_hour(int hour, int minute, char destination[9]) {
    if (hour >= 12) {
        if (hour != 12) {
            hour -= 12;
        }
        sprintf(destination, "%02d:%02d PM", hour, minute);
        
    } else {
        sprintf(destination, "%02d:%02d AM", hour, minute);
    }

    return 0;
}

// parse_time
// purpose: convert the event times into a formatted (12h) and serializable type.
// parameters:
//     start: the event start time.
//     end: the event end time.
//     start_time: the destination string for the formatted start time.
//     end_time: the destination string for the formatted end time. 
// returns: 0 on success. no errors are expected from this function.
int parse_time(char start[MAX_LINE_LEN], char end[MAX_LINE_LEN], char start_time[9], char end_time[9]) {
    char start_hour_string[3];
    char end_hour_string[3];
    char start_minute_string[3];
    char end_minute_string[3];
    char parsed_start[9];
    char parsed_end[9];

    strncpy(start_hour_string, &start[0], 2);
    strncpy(end_hour_string, &end[0], 2);
    strncpy(start_minute_string, &start[3], 2);
    strncpy(end_minute_string, &end[3], 2);
    start_hour_string[2] = '\0';
    end_hour_string[2] = '\0';
    start_minute_string[2] = '\0';
    end_minute_string[2] = '\0';

    int start_hour = atoi(start_hour_string);
    int end_hour = atoi(end_hour_string);
    int start_minute = atoi(start_minute_string);
    int end_minute = atoi(end_minute_string);

    twelve_hour(start_hour, start_minute, start_time);
    twelve_hour(end_hour, end_minute, end_time);

    return 0;
}

// print_event
// purpose: prints the events according to the assignment specification.
// parameters:
//     current_event: the event that has the possibility of being printed on this iteration.
//     last_event: the event chronologically right before the current event. used to determine
//                 if a new date string needs to be printed and for managing new lines.
//     start: the time_t value of the user inputted start date.
//     end: the time_t value of the user inputted end date.
// returns: 0 on success. no errors are expected from this function.
int print_event(char current_event[9][MAX_LINE_LEN], char last_event[9][MAX_LINE_LEN], time_t start, time_t end) {
    long int start_diff = 0, end_diff = 0;
    char current_date_string[11];
    char last_date_string[11];
    char* date_prefix = "\0";

    strcpy(current_date_string, current_event[5]); 
    strcat(current_date_string, "/");
    strcat(current_date_string, current_event[4]);
    strcat(current_date_string, "/");
    strcat(current_date_string, current_event[3]);
    strcat(current_date_string, "\0");

    if (last_event[0][0]) {
        if (last_event[9][0] == 'y') {
            date_prefix = "\n";
        }
        strcpy(last_date_string, last_event[5]);
        strcat(last_date_string, "/");
        strcat(last_date_string, last_event[4]);
        strcat(last_date_string, "/");
        strcat(last_date_string, last_event[3]);
        strcat(last_date_string, "\0");
    }


    struct tm event_date = parse_date(current_date_string);
    time_t event_time = mktime(&event_date);

    char start_time[9];
    char end_time[9];
    parse_time(current_event[7], current_event[8], start_time, end_time);

    char month[10];

    month_name(atoi(current_event[4]), month);

    // Will be positive if the event is after the start date.
    start_diff = event_time - start;
    // Will be positive if the event is before the end date.
    end_diff = end - event_time;

    if (start_diff >= 0 && end_diff >= 0) {
            char date_line[MAX_LINE_LEN];
            if (strcmp(current_date_string, last_date_string) != 0) {
                sprintf(date_line, "%s %s, %s (%s)\n", month, current_event[3], current_event[5], current_event[6]); 
                printf("%s%s", date_prefix, date_line);
                for (int i = 0; i < strlen(date_line) - 1; i++) {
                    printf("-");
                }
                printf("\n");
            }
            printf("%s to %s: %s {{%s}} | %s\n", start_time, end_time, current_event[0], current_event[2], current_event[1]);
            current_event[9][0] = 'y';
    }

    return 0;
}

// parse_event
// purpose: transform the events from the file into a more flexible structure.
// parameters:
//     raw_event: an array of size 11 representing an event as read from the file.
//                the first and last members of the array are the opening and closing <event> tags.
//     parsed_event: an array of size 10 to store the data between the tags of the raw_event.
//                   the 10th member of the array is a character used to track if the event has been printed.
// returns: 0 on success. no errors are expected from this function.
int parse_event(char raw_event[11][MAX_LINE_LEN], char parsed_event[10][MAX_LINE_LEN]) {
   // Iterators for the embedded loop. Yeah.
   int i, j, k;

   // First and last entries of each event array are the opening and closing <event> tags.
   for (i = 1; i < 10; i++) {
           // Start and end of text within tags.
           int start = 0;
           int end = 0;
           for (j = 0; j < MAX_LINE_LEN; j++) {
                   // Find end of opening tag.
                   if (raw_event[i][j] == '>') {
                           start = j + 1;
                           break; 
                   }
           }

           for (j = start; j < MAX_LINE_LEN; j++) {
                   // Find beginning of closing tag.
                   if (raw_event[i][j] == '<') {
                           end = j - 1;
                           break;
                   }
           }

           strcmp(parsed_event[i], raw_event[i]);
           for (j = start; j <= end; j++) {
               parsed_event[i - 1][j - start] = raw_event[i][j];
           }
           parsed_event[i - 1][end - start + 1] = '\0';
           // printf("%s\n", parsed_event[i - 1]);
   }

   parsed_event[9][0] = 'n';

   return 0; 
}

// parse_file
// purpose: reads the lines from the file and puts the event data into an array.
// parameters:
//     path: the file path as specified by the user.
//     raw_events: an array of size MAX_EVENTS representing each event as read from the file.
// returns: 0 on success or 1 if there is an issue opening the file.
int parse_file(char* path, char raw_events[MAX_EVENTS][11][MAX_LINE_LEN]) {
    FILE* f = fopen(path, "r");
    char line[MAX_LINE_LEN]; 
    int lines_scanned = 0;
    int events_count = 0;
    int i = 0;
    
    if (f == NULL) {
        printf("%s\n", "Error opening file");
        return 1;
    }
    
    while (fgets(line, MAX_LINE_LEN, f) != NULL) {
        if (!isspace(line[0])) {
            // Ignore opening and closing <calendar> tags.
            continue;
        }
        // printf("%d %s", lines_scanned, line);
        strcpy(raw_events[events_count][lines_scanned], line);
        lines_scanned++;
        if (lines_scanned % 11 == 0) {
            events_count++;
            lines_scanned = 0;
        }
    }

    fclose(f); 
    return events_count;
}

// swap_events
// purpose: to swap the position of two event elements.
//          used while sorting the events chronologically.
// parameters:
//     event_1, event_2: parsed event arrays of size 10.
// returns: 0 on success. no errors are expected from this function.
int swap_events(char event_1[10][MAX_LINE_LEN], char event_2[10][MAX_LINE_LEN]) {
   int k;
   char temp_event[10][MAX_LINE_LEN]; 
   for (k = 0; k < 10; k++) {
       strcpy(temp_event[k], event_1[k]);
       strcpy(event_1[k], event_2[k]);
       strcpy(event_2[k], temp_event[k]);
   }
   return 0;
}

/*
    Function: main
    Description: represents the entry point of the program.
    Inputs: 
        - argc: indicates the number of arguments to be passed to the program.
        - argv: an array of strings containing the arguments passed to the program.
    Output: an integer describing the result of the execution of the program:
        - 0: Successful execution.
        - 1: Erroneous execution.
*/
int main(int argc, char *argv[])
{
    /* Starting calling your own code from this point. */
    // Ideally, please try to decompose your solution into multiple functions that are called from a concise main() function.
    if (argc < 4 || argc >= 5) {
         printf("%s\n", "Usage: --start=yyyy/m/d --end=yyyy/m/d --file=/path/to/file");
         exit(1);
    }

    int i = 0;
    int j = 0;
    int k = 0;
    strtok(argv[1], "=");
    char* start = strtok(NULL, "=");
    strtok(argv[2], "=");
    char* end = strtok(NULL, "=");
    strtok(argv[3], "=");
    char* file = strtok(NULL, "=");

    char raw_events[MAX_EVENTS][11][MAX_LINE_LEN];
    int events_count = 0;
    struct tm start_date = parse_date(start);
    struct tm end_date = parse_date(end);
    time_t start_time = mktime(&start_date);
    time_t end_time = mktime(&end_date);
    events_count = parse_file(file, raw_events);
    char parsed_events[events_count + 1][10][MAX_LINE_LEN];
    parsed_events[events_count][0][0] = '\0';
    // Parse each event.
    for (i = 0; i < events_count; i++) {
        parse_event(raw_events[i], parsed_events[i]);
    }
    // Sort events chronologically.
    for (i = 0; i < events_count; i++) {
        for (j = i + 1; j < events_count; j++) {
            char outer_start_hour[3];
            char inner_start_hour[3];
            char outer_start_minute[3];
            char inner_start_minute[3];

            strncpy(outer_start_hour, &parsed_events[i][7][0], 2);
            strncpy(outer_start_minute, &parsed_events[i][7][3], 2);
            strncpy(inner_start_hour, &parsed_events[j][7][0], 2);
            strncpy(inner_start_minute, &parsed_events[j][7][3], 2);

            if (atoi(parsed_events[i][5]) > atoi(parsed_events[j][5])) {
                //year
                swap_events(parsed_events[i], parsed_events[j]);
            } else if (atoi(parsed_events[i][5]) < atoi(parsed_events[j][5])) {
                continue;
            } else if (atoi(parsed_events[i][4]) > atoi(parsed_events[j][4])) {
                //month
                swap_events(parsed_events[i], parsed_events[j]);
            } else if (atoi(parsed_events[i][4]) < atoi(parsed_events[j][4])) {
                continue;
            } else if (atoi(parsed_events[i][3]) > atoi(parsed_events[j][3])) {
                //day
                swap_events(parsed_events[i], parsed_events[j]);
            } else if (atoi(parsed_events[i][3]) < atoi(parsed_events[j][3])) {
                continue;
            } else if (atoi(outer_start_hour) > atoi(inner_start_hour)) {
                //hour
                swap_events(parsed_events[i], parsed_events[j]);
                //break;
            } else if (atoi(outer_start_hour) < atoi(inner_start_hour)) {
                continue;
            } else if (atoi(outer_start_minute) > atoi(inner_start_minute)) {
                //minute
                swap_events(parsed_events[i], parsed_events[j]);
            } else if (atoi(outer_start_minute) < atoi(inner_start_minute)) {
                continue;
            }
        }       
    }
    // Print events.
    for (i = 0; i < events_count; i++) {
        if (i != 0) {
            print_event(parsed_events[i], parsed_events[i - 1], start_time, end_time);
        } else {
            // parsed_events[events_count][0][0] is null. It is not an event.
            print_event(parsed_events[i], parsed_events[events_count], start_time, end_time);
        }
    } 

    exit(0);
}

